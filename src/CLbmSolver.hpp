/*
 * Copyright
 * 2010 Martin Schreiber
 * 2013 Arash Bakhtiari
 * 2016 Christoph Riesinger, Ayman Saleem
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CLBMSOLVER_HPP
#define CLBMSOLVER_HPP

#include <vector>

#include "libmath/CVector.hpp"
#include "CConfiguration.hpp"
#include "CDomain.hpp"
#include "common.h"

template<typename T>
class CLbmSolver
{
protected:
    int id;
    CDomain<T> domain;
    /*
     * Vector for the boundary conditions of the six faces of a cuboid.
     * boundaryConditions[0]: boundary condition for left face (smallest x-coordinate)
     * boundaryConditions[1]: boundary condition for right face (largest x-coordinate)
     * boundaryConditions[2]: boundary condition for bottom face (smallest y-coordinate)
     * boundaryConditions[3]: boundary condition for top face (largest y-coordinate)
     * boundaryConditions[4]: boundary condition for back face (smallest z-coordinate)
     * boundaryConditions[5]: boundary condition for front face (largest z-coordinate)
     */
    std::vector<Flag> boundaryConditions;
    CConfiguration<T>* configuration;

    T* densityDistributions;
    Flag* flags;
    T* velocities;
    T* densities;

    T timestepSize;
    CVector<3, T> velocityDimLess;
    CVector<3, T> accelerationDimLess;
    T viscosity, viscosityDimLess;
    T tau;

    bool storeDensities;
    bool storeVelocities;

    T tauInv;

public:
    CLbmSolver();
    CLbmSolver(
            int id,
            CDomain<T> &domain,
            std::vector<Flag> boundaryConditions,
            CConfiguration<T>* configuration);
    virtual ~CLbmSolver() {}
    
    virtual void simulationStepAlpha() {}
    virtual void simulationStepAlpha(CVector<3, int> origin, CVector<3, int> size) {}
    virtual void simulationStepBeta() {}
    virtual void simulationStepBeta(CVector<3, int> origin, CVector<3, int> size) {}
    CDomain<T>* getDomain();
    virtual void getDensityDistributions(CVector<3, int> &origin, CVector<3, int> &size, Architecture dstArch, T* dst, cudaStream_t* stream = NULL) {}
    virtual void getDensityDistributions(Architecture dstArch, T* dst, cudaStream_t* stream = NULL) {}
    T* getDensityDistributions();
    virtual void setDensityDistributions(CVector<3, int> &origin, CVector<3, int> &size, Architecture srcArch, T* src, cudaStream_t* stream = NULL) {}
    virtual void setDensityDistributions(Architecture dstArch, T* src, cudaStream_t* stream = NULL) {}
    virtual void getFlags(CVector<3, int> &origin, CVector<3, int> &size, Architecture dstArch, Flag* dst) {}
    virtual void getFlags(Architecture dstArch, Flag* dst) {}
    Flag* getFlags();
    virtual void setFlags(CVector<3, int> &origin, CVector<3, int> &size, Architecture srcArch, Flag* src) {}
    virtual void setFlags(Architecture srcArch, Flag* src) {}
    virtual void getVelocities(CVector<3, int> &origin, CVector<3, int> &size, Architecture dstArch, T* dst) {}
    virtual void getVelocities(Architecture dstArch, T* dst) {}
    T* getVelocities();
    virtual void setVelocities(CVector<3, int> &origin, CVector<3, int> &size, Architecture srcArch, T* src) {}
    virtual void setVelocities(Architecture srcArch, T* src) {}
    virtual void getDensities(CVector<3, int> &origin, CVector<3, int> &size, Architecture dstArch, T* dst) {}
    virtual void getDensities(Architecture dstArch, T* dst) {}
    T* getDensities();
    virtual void setDensities(CVector<3, int> &origin, CVector<3, int> &size, Architecture srcArch, T* src) {}
    virtual void setDensities(Architecture srcArch, T* src) {}
};

#endif
