/*
 * Copyright
 * 2010 Martin Schreiber
 * 2013 Arash Bakhtiari
 * 2016 Christoph Riesinger, Ayman Saleem
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CVECTOR_HPP
    #error "Don't include CVector4.hpp directly!"
#endif

#ifndef CVECTOR4_HPP
#define CVECTOR4_HPP

#include <iostream>

template <typename T>
class CVector<4, T>
{
public:
    T data[4];

    CVector();
    CVector(const T x);
    CVector(const T x0, const T x1, const T x2, const T x3);
    CVector(const T v[4]);
    CVector(const CVector<4, T>& v);

    int getGlobalIdx(CVector<4, T>& size);
    void set(const T x0, const T x1, const T x2, const T x3);
    void setZero();
    T elements();
    T min();
    T max();
    bool isPositive();
    bool isNegative();
    T length();
    T length2();

    CVector<4, T>& operator=(const T v[4]);
    CVector<4, T>& operator=(const CVector<4, T>& v);
    CVector<4, T> operator+(const T x);
    CVector<4, T> operator+(const CVector<4, T>& v);
    CVector<4, T>& operator+=(const T x);
    CVector<4, T>& operator+=(const CVector<4, T>& v);
    CVector<4, T> operator-(const T x);
    CVector<4, T> operator-(const CVector<4, T>& v);
    CVector<4, T>& operator-=(const T x);
    CVector<4, T>& operator-=(const CVector<4, T>& v);
    CVector<4, T> operator*(const T x);
    CVector<4, T> operator*(const CVector<4, T>& v);
    CVector<4, T>& operator*=(const T x);
    CVector<4, T> operator/(const T x);
    CVector<4, T> operator/(const CVector<4, T>& v);
    CVector<4, T>& operator/=(const T x);
    bool operator==(const CVector<4, T>& v);
    bool operator!=(const CVector<4, T>& v);
    T& operator[](const int i);
};

template <class T>
std::ostream& operator<<(std::ostream& co, const CVector<4, T>& v);

#endif
