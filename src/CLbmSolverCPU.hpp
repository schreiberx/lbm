/*
 * Copyright
 * 2010 Martin Schreiber
 * 2013 Arash Bakhtiari
 * 2016 Christoph Riesinger, Ayman Saleem
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CLBMSOLVERCPU_HPP
#define CLBMSOLVERCPU_HPP

#include "CLbmSolver.hpp"

#include "cpukernels/CLbmInitCPU.hpp"
#include "cpukernels/CLbmAlphaCPU.hpp"
#include "cpukernels/CLbmBetaCPU.hpp"
#include "CComm.hpp"

template<typename T>
class CLbmSolverCPU : public CLbmSolver<T>
{
private:
    using CLbmSolver<T>::id;
    using CLbmSolver<T>::domain;
    using CLbmSolver<T>::configuration;
    using CLbmSolver<T>::densityDistributions;
    using CLbmSolver<T>::flags;
    using CLbmSolver<T>::velocities;
    using CLbmSolver<T>::densities;
    using CLbmSolver<T>::velocityDimLess;
    using CLbmSolver<T>::accelerationDimLess;
    using CLbmSolver<T>::storeDensities;
    using CLbmSolver<T>::storeVelocities;
    using CLbmSolver<T>::tauInv;

    CLbmInitCPU<T>* initLbmCPU;
    CLbmAlphaCPU<T>* alphaLbmCPU;
    CLbmBetaCPU<T>* betaLbmCPU;

    void getVariable(CVector<3, int>& size, CVector<3, int>& srcOrigin, CVector<3, int>& dstOrigin, CVector<3, int>& dstDim, Architecture dstArch, T* src, T* dst, cudaStream_t* stream);
    void getVariable(CVector<3, int>& size, CVector<3, int>& srcOrigin, CVector<3, int>& dstOrigin, CVector<3, int>& dstDim, Architecture dstArch, Flag* src, Flag* dst, cudaStream_t* stream);
    void setVariable(CVector<3, int>& size, CVector<3, int>& srcOrigin, CVector<3, int>& srcDim, Architecture srcArch, CVector<3, int>& dstOrigin, T* src, T* dst, cudaStream_t* stream);
    void setVariable(CVector<3, int>& size, CVector<3, int>& srcOrigin, CVector<3, int>& srcDim, Architecture srcArch, CVector<3, int>& dstOrigin, Flag* src, Flag* dst, cudaStream_t* stream);
public:
    CLbmSolverCPU();
    CLbmSolverCPU(
            int id,
            CDomain<T>& domain,
            std::vector<Flag> boundaryConditions,
            CConfiguration<T>* configuration);
    ~CLbmSolverCPU();

    using CLbmSolver<T>::simulationStepAlpha;
    using CLbmSolver<T>::simulationStepBeta;
    using CLbmSolver<T>::getDensityDistributions;
    using CLbmSolver<T>::setDensityDistributions;

    void simulationStepAlpha();
    void simulationStepAlpha(CVector<3, int> origin, CVector<3, int> size);
    void simulationStepBeta();
    void simulationStepBeta(CVector<3, int> origin, CVector<3, int> size);
    void getDensityDistributions(CVector<3, int>& origin, CVector<3, int>& size, Architecture dstArch, T* dst, cudaStream_t* stream = NULL);
    void getDensityDistributions(Architecture dstArch, T* dst, cudaStream_t* stream = NULL);
    void setDensityDistributions(CVector<3, int>& size, CVector<3, int>& srcOrigin, CVector<3, int>& srcDim, Architecture srcArch, CVector<3, int>& dstOrigin, Direction direction, T* src, cudaStream_t* stream = NULL);
    void setDensityDistributions(CVector<3, int>& size, CVector<3, int>& srcOrigin, CVector<3, int>& srcDim, Architecture srcArch, CVector<3, int>& dstOrigin, T* src, cudaStream_t* stream = NULL);
    void setDensityDistributions(CVector<3, int>& origin, CVector<3, int>& size, Architecture srcArch, Direction direction, T* src, cudaStream_t* stream = NULL);
    void setDensityDistributions(CVector<3, int>& origin, CVector<3, int>& size, Architecture srcArch, T* src, cudaStream_t* stream = NULL);
    void setDensityDistributions(Architecture srcArch, T* src, cudaStream_t* stream = NULL);
    void getFlags(CVector<3, int>& origin, CVector<3, int>& size, Architecture dstArch, Flag* dst);
    void getFlags(Architecture dstArch, Flag* dst);
    void setFlags(CVector<3, int>& origin, CVector<3, int>& size, Architecture srcArch, Flag* src);
    void setFlags(Architecture srcArch, Flag* src);
    void getVelocities(CVector<3, int>& origin, CVector<3, int>& size, Architecture dstArch, T* dst);
    void getVelocities(Architecture dstArch, T* dst);
    void setVelocities(CVector<3, int>& origin, CVector<3, int>& size, Architecture srcArch, T* src);
    void setVelocities(Architecture srcArch, T* src);
    void getDensities(CVector<3, int>& origin, CVector<3, int>& size, Architecture dstArch, T* dst);
    void getDensities(Architecture dstArch, T* dst);
    void setDensities(CVector<3, int>& origin, CVector<3, int>& size, Architecture srcArch, T* src);
    void setDensities(Architecture srcArch, T* src);
};

#endif
