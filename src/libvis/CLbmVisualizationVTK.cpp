/*
 * Copyright
 * 2010 Martin Schreiber
 * 2013 Arash Bakhtiari
 * 2016 Christoph Riesinger, Ayman Saleem
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "CLbmVisualizationVTK.hpp"

#include <typeinfo>

template <class T>
CLbmVisualizationVTK<T>::CLbmVisualizationVTK(
        int id,
        int visualizationRate,
        CLbmSolverCPU<T>* solverCPU,
        CLbmSolverGPU<T>* solverGPU,
        std::string filePath) :
        CLbmVisualization<T>(id, visualizationRate, solverCPU, solverGPU),
        filePath(filePath)
{
}

template <class T>
void CLbmVisualizationVTK<T>::openFile(int iteration)
{
    if (!file.is_open())
    {
        std::stringstream fileName;
        fileName << filePath << "/visualization_" << id << "_" << iteration << ".vtk";
        file.open(fileName.str().c_str(), std::ios::out);
    } else {
        std::cerr << "----- CLbmVisualizationVTK<T>::openFile() -----" << std::endl;
        std::cerr << "VTK file \"visualization_" << id << "_" << iteration << ".vtk is already open." << std::endl;
        std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
        std::cerr << "-----------------------------------------------" << std::endl;

        exit (EXIT_FAILURE);
    }
}

template <class T>
void CLbmVisualizationVTK<T>::closeFile()
{
    if (file.is_open())
    {
        file.close();
    } else {
        std::cerr << "----- CLbmVisualizationVTK<T>::closeFile() -----" << std::endl;
        std::cerr << "There is no open VTK file to close." << std::endl;
        std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
        std::cerr << "------------------------------------------------" << std::endl;

        exit (EXIT_FAILURE);
    }
}

template <class T>
void CLbmVisualizationVTK<T>::writeHeader()
{
    if (file.is_open())
    {
        file << "# vtk DataFile Version 3.0\n";
        file << "Heterogeneous (CPU/GPU) and hybrid (MPI/OpenMP/CUDA) LBM simulation on rank " << id << "\n";
        file << "ASCII" << std::endl;
    } else {
        std::cerr << "----- CLbmVisualizationVTK<T>::writeHeader() -----" << std::endl;
        std::cerr << "There is no open VTK file to write header" << std::endl;
        std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
        std::cerr << "--------------------------------------------------" << std::endl;

        exit (EXIT_FAILURE);
    }
}

template <class T>
void CLbmVisualizationVTK<T>::writeDataset()
{
    if (file.is_open())
    {
        file << "DATASET STRUCTURED_GRID\n";
        file << "DIMENSIONS " << (solverCPU->getDomain()->getSize()[0] + 1) << " " << (solverCPU->getDomain()->getSize()[1] + solverGPU->getDomain()->getSize()[1] + 1) << " " << (solverCPU->getDomain()->getSize()[2] + 1) << "\n";
        file << "POINTS " << ((solverCPU->getDomain()->getSize()[0] + 1) * (solverCPU->getDomain()->getSize()[1] + solverGPU->getDomain()->getSize()[1] + 1) * (solverCPU->getDomain()->getSize()[2] + 1)) << " " << ((typeid(T) == typeid(float)) ? "float" : "double") << "\n";

        for (int k = 0; k < solverCPU->getDomain()->getSize()[2] + 1; k++) {
            for (int j = 0; j < solverCPU->getDomain()->getSize()[1] + solverGPU->getDomain()->getSize()[1] + 1; j++) {
                for (int i = 0; i < solverCPU->getDomain()->getSize()[0] + 1; i++) {
                    file << ((T)(solverCPU->getDomain()->getOrigin()[0] + i) * solverCPU->getDomain()->getLength()[0] / (T)solverCPU->getDomain()->getSize()[0]) << " " <<
                            ((T)(solverCPU->getDomain()->getOrigin()[1] + j) * (solverCPU->getDomain()->getLength()[1] + solverGPU->getDomain()->getLength()[1]) / (T)(solverCPU->getDomain()->getSize()[1] + solverGPU->getDomain()->getSize()[1])) << " " <<
                            ((T)(solverCPU->getDomain()->getOrigin()[2] + k) * solverCPU->getDomain()->getLength()[2] / (T)solverCPU->getDomain()->getSize()[2]) <<"\n";
                }
            }
        }
    } else {
        std::cerr << "----- CLbmVisualizationVTK<T>::writeDataset() -----" << std::endl;
        std::cerr << "There is no open VTK file to write dataset" << std::endl;
        std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
        std::cerr << "---------------------------------------------------" << std::endl;

        exit (EXIT_FAILURE);
    }
}

template <class T>
void CLbmVisualizationVTK<T>::writeFlags()
{
    if (file.is_open())
    {
        int idx;

        if (solverCPU->getDomain()->hasDomain())
            solverCPU->getFlags(CPU, flagsCPU);
        if (solverGPU->getDomain()->hasDomain())
            solverGPU->getFlags(CPU, flagsGPU);

        file << "SCALARS flags INT 1\n";
        file << "LOOKUP_TABLE default\n";

        for (int k = 0; k < solverCPU->getDomain()->getSize()[2]; k++) {
            for (int j = 0; j < solverCPU->getDomain()->getSize()[1]; j++) {
                for (int i = 0; i < solverCPU->getDomain()->getSize()[0]; i++) {
                    idx = k * (solverCPU->getDomain()->getSize()[0] * solverCPU->getDomain()->getSize()[1]) + j * solverCPU->getDomain()->getSize()[0] + i;

                    file << flagsCPU[idx] << "\n";
                }
            }
            for (int j = 0; j < solverGPU->getDomain()->getSize()[1]; j++) {
                for (int i = 0; i < solverGPU->getDomain()->getSize()[0]; i++) {
                    idx = k * (solverGPU->getDomain()->getSize()[0] * solverGPU->getDomain()->getSize()[1]) + j * solverGPU->getDomain()->getSize()[0] + i;

                    file << flagsGPU[idx] << "\n";
                }
            }
        }
    } else {
        std::cerr << "----- CLbmVisualizationVTK<T>::writeFlags() -----" << std::endl;
        std::cerr << "There is no open VTK file to write flags." << std::endl;
        std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
        std::cerr << "-----------------------------------------------  " << std::endl;

        exit (EXIT_FAILURE);
    }
}

template <class T>
void CLbmVisualizationVTK<T>::writeDensities()
{
    if (file.is_open())
    {
        int idx;

        if (solverCPU->getDomain()->hasDomain())
            solverCPU->getDensities(CPU, densitiesCPU);
        if (solverGPU->getDomain()->hasDomain())
            solverGPU->getDensities(CPU, densitiesGPU);

        file << "SCALARS densities " << ((typeid(T) == typeid(float)) ? "float" : "double") << " 1\n";
        file << "LOOKUP_TABLE default\n";

        for (int k = 0; k < solverCPU->getDomain()->getSize()[2]; k++) {
            for (int j = 0; j < solverCPU->getDomain()->getSize()[1]; j++) {
                for (int i = 0; i < solverCPU->getDomain()->getSize()[0]; i++) {
                    idx = k * (solverCPU->getDomain()->getSize()[0] * solverCPU->getDomain()->getSize()[1]) + j * solverCPU->getDomain()->getSize()[0] + i;

                    file << densitiesCPU[idx] << "\n";
                }
            }
            for (int j = 0; j < solverGPU->getDomain()->getSize()[1]; j++) {
                for (int i = 0; i < solverGPU->getDomain()->getSize()[0]; i++) {
                    idx = k * (solverGPU->getDomain()->getSize()[0] * solverGPU->getDomain()->getSize()[1]) + j * solverGPU->getDomain()->getSize()[0] + i;

                    file << densitiesGPU[idx] << "\n";
                }
            }
        }
    } else {
        std::cerr << "----- CLbmVisualizationVTK<T>::writeDensities() -----" << std::endl;
        std::cerr << "There is no open VTK file to write densities." << std::endl;
        std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
        std::cerr << "-----------------------------------------------------" << std::endl;

        exit (EXIT_FAILURE);
    }
}

template <class T>
void CLbmVisualizationVTK<T>::writeVelocities()
{
    if (file.is_open())
    {
        int idx;
        T* velocitiesXCPU;
        T* velocitiesYCPU;
        T* velocitiesZCPU;
        T* velocitiesXGPU;
        T* velocitiesYGPU;
        T* velocitiesZGPU;

        if (solverCPU->getDomain()->hasDomain())
            solverCPU->getVelocities(CPU, velocitiesCPU);
        if (solverGPU->getDomain()->hasDomain())
            solverGPU->getVelocities(CPU, velocitiesGPU);

        velocitiesXCPU = velocitiesCPU;
        velocitiesYCPU = velocitiesCPU + solverCPU->getDomain()->getNumOfCells();
        velocitiesZCPU = velocitiesCPU + 2 * solverCPU->getDomain()->getNumOfCells();
        velocitiesXGPU = velocitiesGPU;
        velocitiesYGPU = velocitiesGPU + solverGPU->getDomain()->getNumOfCells();
        velocitiesZGPU = velocitiesGPU + 2 * solverGPU->getDomain()->getNumOfCells();

        file << "VECTORS velocities " << ((typeid(T) == typeid(float)) ? "float" : "double") << "\n";

        for (int k = 0; k < solverCPU->getDomain()->getSize()[2]; k++) {
            for (int j = 0; j < solverCPU->getDomain()->getSize()[1]; j++) {
                for (int i = 0; i < solverCPU->getDomain()->getSize()[0]; i++) {
                    idx = k * (solverCPU->getDomain()->getSize()[0] * solverCPU->getDomain()->getSize()[1]) + j * solverCPU->getDomain()->getSize()[0] + i;

                    file << velocitiesXCPU[idx] << " " << velocitiesYCPU[idx] << " " << velocitiesZCPU[idx] << "\n";
                }
            }
            for (int j = 0; j < solverGPU->getDomain()->getSize()[1]; j++) {
                for (int i = 0; i < solverGPU->getDomain()->getSize()[0]; i++) {
                    idx = k * (solverGPU->getDomain()->getSize()[0] * solverGPU->getDomain()->getSize()[1]) + j * solverGPU->getDomain()->getSize()[0] + i;

                    file << velocitiesXGPU[idx] << " " << velocitiesYGPU[idx] << " " << velocitiesZGPU[idx] << "\n";
                }
            }
        }
    } else {
        std::cerr << "----- CLbmVisualizationVTK<T>::writeVelocities() -----" << std::endl;
        std::cerr << "There is no open VTK file to write velocities." << std::endl;
        std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
        std::cerr << "------------------------------------------------------" << std::endl;

        exit (EXIT_FAILURE);
    }
}

template <class T>
void CLbmVisualizationVTK<T>::render(int iteration)
{
    if(iteration % visualizationRate == 0)
    {
        openFile(iteration);
        writeHeader();
        writeDataset();
        file << "CELL_DATA " << (solverCPU->getDomain()->getNumOfCells() + solverGPU->getDomain()->getNumOfCells()) << "\n";
        writeFlags();
        writeDensities();
        writeVelocities();
        closeFile();

    }
}

template class CLbmVisualizationVTK<double>;
template class CLbmVisualizationVTK<float>;
