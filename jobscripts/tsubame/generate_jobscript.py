#!/usr/bin/python
import sys

numOfThreadsPerNode = 12

code =  '#!/bin/sh\n'
code += '#\n'
code += '#PBS -e ' + str(sys.argv[13]) + '/workspace/lbm/results/' + str(sys.argv[12]) + '_' + str(sys.argv[1]) + 'x' + str(sys.argv[2]) + 'x' + str(sys.argv[3]) + '_' + str(sys.argv[4]) + 'x' + str(sys.argv[5]) + 'x' + str(sys.argv[6]) + '.e.txt\n'
code += '#PBS -o ' + str(sys.argv[13]) + '/workspace/lbm/results/' + str(sys.argv[12]) + '_' + str(sys.argv[1]) + 'x' + str(sys.argv[2]) + 'x' + str(sys.argv[3]) + '_' + str(sys.argv[4]) + 'x' + str(sys.argv[5]) + 'x' + str(sys.argv[6]) + '.o.txt\n'
code += '#\n'
code += '#PBS -m ae\n'
code += '#PBS -M riesinge@in.tum.de\n'
code += '\n'
code += 'source set_intel-2015.2.164.sh\n'
code += 'source set_ompi-1.8.2_i2013.1.046_cuda7.5.sh\n'
# code += 'source set_mvp-2.0rc1_i2013.1.046_cuda7.5.sh\n'
code += '\n'
code += 'export LD_LIBRARY_PATH=${HOME}/lib/netcdf4/lib:${LD_LIBRARY_PATH}\n'
# code += 'export MV2_CPU_MAPPING='
# for i in range(0, int(sys.argv[8])):
# 	for j in range(i, numOfThreadsPerNode, int(sys.argv[8])):
# 		code += str(j)
# 		if (j < numOfThreadsPerNode - int(sys.argv[8])):
# 			code += ','
# 	if (i < int(sys.argv[8]) - 1):
# 		code += ':'
# code += '\n'
code += 'export OMP_NUM_THREADS=' + str(sys.argv[11]) + '\n'
# fine:          One software thread is bound to one hardware thread without any
#                context switching.
# compact:       
# first number:  Offset between threads. 1 means that always one hadrware thread
#                is skipped which means, hyperthreads are skipped.
# second number: Offset for first thread. 0 means, that 0th thread is mapped to
#                0th core.
code += 'export KMP_AFFINITY=granularity=fine,compact,1,0\n'
# code += 'export KMP_AFFINITY=verbose,granularity=fine,compact,1,0\n'
code += '\n'
# GPU-only
# for i in range(0, 3):
code += 'mpirun -n ' + str(sys.argv[12]) + ' -hostfile ${PBS_NODEFILE} -x OMP_NUM_THREADS=${OMP_NUM_THREADS} --bind-to core --map-by ppr:' + str(sys.argv[8]) + ':node:pe=' + str(sys.argv[11]) + ' ' + str(sys.argv[13]) + '/workspace/lbm/bin/lbm ' + str(sys.argv[13]) + '/workspace/lbm/configurations/tsubame_' + str(int(sys.argv[12])) + '_' + str(sys.argv[1]) + 'x' + str(sys.argv[2]) + 'x' + str(sys.argv[3]) + '_' + str(sys.argv[4]) + 'x' + str(sys.argv[5]) + 'x' + str(sys.argv[6]) + '_0.0.xml\n'
# CPU-only
# for i in range(0, 3):
code += 'mpirun -n ' + str(sys.argv[12]) + ' -hostfile ${PBS_NODEFILE} -x OMP_NUM_THREADS=${OMP_NUM_THREADS} --bind-to core --map-by ppr:' + str(sys.argv[8]) + ':node:pe=' + str(sys.argv[11]) + ' ' + str(sys.argv[13]) + '/workspace/lbm/bin/lbm ' + str(sys.argv[13]) + '/workspace/lbm/configurations/tsubame_' + str(int(sys.argv[12])) + '_' + str(sys.argv[1]) + 'x' + str(sys.argv[2]) + 'x' + str(sys.argv[3]) + '_' + str(sys.argv[4]) + 'x' + str(sys.argv[5]) + 'x' + str(sys.argv[6]) + '_1.0.xml\n'
# heterogeneous
for ratio in range(15, 0, -1):
	code += 'mpirun -n ' + str(sys.argv[12]) + ' -hostfile ${PBS_NODEFILE} -x OMP_NUM_THREADS=${OMP_NUM_THREADS} --bind-to core --map-by ppr:' + str(sys.argv[8]) + ':node:pe=' + str(sys.argv[11]) + ' ' + str(sys.argv[13]) + '/workspace/lbm/bin/lbm ' + str(sys.argv[13]) + '/workspace/lbm/configurations/tsubame_' + str(int(sys.argv[12])) + '_' + str(sys.argv[1]) + 'x' + str(sys.argv[2]) + 'x' + str(sys.argv[3]) + '_' + str(sys.argv[4]) + 'x' + str(sys.argv[5]) + 'x' + str(sys.argv[6]) + '_' + str(float(ratio) / float(100)) + '.xml\n'
# code += 'mpirun -n ' + str(sys.argv[12]) + ' -hostfile ${PBS_NODEFILE} -x OMP_NUM_THREADS=${OMP_NUM_THREADS} --report-bindings  --rank-by node --bind-to core --map-by ppr:' + str(sys.argv[8]) + ':node:pe=' + str(sys.argv[11]) + ' ' + str(sys.argv[13]) + '/workspace/testcpp/bin/hybrid_affinity\n'

jobscript = open('jobscript.sh', 'w')
jobscript.write(code)
jobscript.close()

