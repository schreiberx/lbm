#!/usr/bin/python
import sys

code =  "# @ shell=/bin/bash\n";
code += "#\n";
code += "# @ error            = " + str(sys.argv[10]) + "/workspace/lbm/results/" + str(int(sys.argv[7]) * int(sys.argv[8])) + "_" + str(sys.argv[1]) + "x" + str(sys.argv[2]) + "x" + str(sys.argv[3]) + "_" + str(sys.argv[4]) + "x" + str(sys.argv[5]) + "x" + str(sys.argv[6]) + ".e.txt\n";
code += "# @ output           = " + str(sys.argv[10]) + "/workspace/lbm/results/" + str(int(sys.argv[7]) * int(sys.argv[8])) + "_" + str(sys.argv[1]) + "x" + str(sys.argv[2]) + "x" + str(sys.argv[3]) + "_" + str(sys.argv[4]) + "x" + str(sys.argv[5]) + "x" + str(sys.argv[6]) + ".o.txt\n";
code += "# @ job_type         = parallel\n";
code += "# @ requirements     = (Feature==\"gpu\")\n";
code += "# @ node_usage       = not_shared\n";
code += "#\n";
code += "# Number of nodes:\n";
code += "# @ node             = " + str(sys.argv[7]) + "\n";
code += "# Number of tasks/ranks/processes per node:\n";
code += "# @ tasks_per_node   = " + str(sys.argv[8]) + "\n";
code += "# Number of threads per task/rank/process:\n";
code += "# @ resources        = ConsumableCpus(" + str(sys.argv[9]) + ")\n";
code += "# @ wall_clock_limit = 06:00:00\n";
code += "#\n";
code += "# @ network.MPI      = sn_all,not_shared,us\n";
code += "# @ notification     = complete\n";
code += "# @ notify_user      = $(user)@rzg.mpg.de\n";
code += "# @ queue\n";
code += "\n";
code += "module load intel/16.0\n";
code += "module load cuda/7.5\n";
code += "module unload mpi.ibm/1.4.0\n";
code += "module load mpi.intel/5.1.3\n";
code += "module load netcdf-mpi/4.4.0\n";
code += "\n";
code += "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$NETCDF_HOME/lib/\n";
code += "export OMP_NUM_THREADS=" + str(sys.argv[9]) + "\n";
code += "export KMP_AFFINITY=granularity=fine,compact,1,0\n";
code += "\n";
code += "poe ${HOME}/workspace/lbm/bin/lbm ${HOME}/workspace/lbm/configurations/hydra_" + str(int(sys.argv[7]) * int(sys.argv[8])) + "_" + str(sys.argv[1]) + "x" + str(sys.argv[2]) + "x" + str(sys.argv[3]) + "_0.0.xml\n";
for ratio in xrange(10, 31, 1):
	code += "poe ${HOME}/workspace/lbm/bin/lbm ${HOME}/workspace/lbm/configurations/hydra_" + str(int(sys.argv[7]) * int(sys.argv[8])) + "_" + str(sys.argv[1]) + "x" + str(sys.argv[2]) + "x" + str(sys.argv[3]) + "_" + str(float(ratio) / float(100)) + ".xml\n";
code += "poe ${HOME}/workspace/lbm/bin/lbm ${HOME}/workspace/lbm/configurations/hydra_" + str(int(sys.argv[7]) * int(sys.argv[8])) + "_" + str(sys.argv[1]) + "x" + str(sys.argv[2]) + "x" + str(sys.argv[3]) + "_1.0.xml\n";
	
jobscript = open("jobscript.sh", "w")
jobscript.write(code)
jobscript.close()

